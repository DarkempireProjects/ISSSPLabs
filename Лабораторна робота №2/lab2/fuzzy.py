from abc import ABCMeta, abstractmethod


class FuzzyVariables:
    __metaclass__ = ABCMeta

    def __init__(self, fuzzydict):
        self.fuzzydict = fuzzydict

    @abstractmethod
    def to_interval(self, value, alpha=0):
        pass

    def generate_level_converter(self, alpha):

        def to_interval(x):
            if isinstance(x, int):
                return x, x
            if isinstance(x, str):
                x = x[1:]
                make_reverse = False
                if x.startswith('1/'):
                    x = x[2:]
                    make_reverse = True
                x = self.to_interval(x, alpha)
                if make_reverse:
                    x = (round(1 / x[1], 2), round(1 / x[0], 2))
                return x

        return to_interval


class TriangleFuzzyVariables(FuzzyVariables):

    def to_interval(self, value, alpha=0):
        a, b, c = self.fuzzydict.get(value)
        return a + alpha * (b - a), c - alpha * (c - b)


class TrapezeFuzzyVariables(FuzzyVariables):

    def to_interval(self, value, alpha=0):
        a, b, c, d = self.fuzzydict.get(value)
        return a + alpha * (b-a), d - alpha * (d - c)

