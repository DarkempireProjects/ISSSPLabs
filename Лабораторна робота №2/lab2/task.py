
class FuzzyMatrixPairwiseComparisons:

    def __init__(self, c_fmpc, a_fmpcs):
        self.c_fmpc = c_fmpc
        self.a_fmpcs = a_fmpcs

    def build_interval_matrices(self, fuzzy_variables, alpha):
        level_converter = fuzzy_variables.generate_level_converter(alpha)
        c_impc = self.c_fmpc.applymap(level_converter)
        a_impcs = [a_fmpc.applymap(level_converter) for a_fmpc in self.a_fmpcs]
        return IntervalMatrixPairwiseComparisons(alpha, c_impc, a_impcs)


class IntervalMatrixPairwiseComparisons:

    def __init__(self, alpha, c_impc, a_impcs):
        self.alpha = alpha
        self.c_impc = c_impc
        self.a_impcs = a_impcs

    def check_consistency(self):
        print('Check consistency for C matrix')
        self.check_consistency_for_matrix(self.c_impc)
        for i in range(0, len(self.a_impcs)):
            print('Check consistency for A_{} matrix'.format(i+1))
            self.check_consistency_for_matrix(self.a_impcs[i])

    @staticmethod
    def check_consistency_for_matrix(df):
        column_count = len(df.columns)
        row_count = len(df.index)
        for i in range(0, column_count):
            for j in range(0, row_count):
                l_max = df[i][0][0] * df[0][j][0]
                u_min = df[i][0][1] * df[0][j][1]
                for k in range(1, column_count):
                    l_max = max(l_max, df[i][k][0] * df[k][j][0])
                    u_min = min(u_min, df[i][k][1] * df[k][j][1])
                if l_max > u_min:
                    print('Problem with i={} j={} , data: l_min={} u_max={} '.format(i, j, l_max, u_min))
                    return False
        return True
