def gen_trimf(a, b, c, d):
    def trimf(x):
        if x < a:
            return 0
        if a <= x <= b:
            return (x - a) / (b - a)
        if b < x <= c:
            return 1
        if c < x <= d:
            return (d - x) / (d - c)
        return 0

    return trimf
