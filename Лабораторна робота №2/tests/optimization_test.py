import numpy as np
from scipy.optimize import minimize


def generate(matrix):
    cons = []
    n = len(matrix)
    func = lambda x: sum(x[n:])
    for i in range(0, n - 1):
        for j in range(i, n):
            cons.append({
                'type': 'ineq',
                'fun': lambda x: np.array([x[i] / x[j] - (1 - x[n + 2 * i * (n-j) + 2 * j]) * matrix[i][j][0]])
            })
            cons.append({
                'type': 'ineq',
                'fun': lambda x: np.array([(1 + x[n + 2 * i * (n-j) + 2 * j + 1]) * matrix[i][j][1] - x[i] / x[j]])
            })
            cons.append({
                'type': 'ineq',
                'fun': lambda x: np.array([x[n + 2 * i * (n-j) + 2 * j]])
            })
            cons.append({
                'type': 'ineq',
                'fun': lambda x: np.array([x[n + 2 * i * (n-j) + 2 * j+1]])
            })
            cons.append({
                'type': 'ineq',
                'fun': lambda x: np.array([1-x[n + 2 * i * (n-j) + 2 * j]])
            })
        cons.append({
            'type': 'ineq',
            'fun': lambda x: np.array([x[i]])
        })
        cons.append({
            'type': 'ineq',
            'fun': lambda x: np.array([1-x[i]])
        })

    return cons, func


matrix = [
    [(1, 1), (4, 6), (1, 1), (2, 4)],
    [(0, 167, 0.25), (1, 1), (0.25, 0.5), (0.25, 0.5)],
    [(1, 1), (2, 4), (1, 1), (1, 1)],
    [(0.25, 0.5), (2, 4), (1, 1), (1, 1)]
]
print(len(matrix))
cons, func = generate(matrix)
x0 = [0.5 for i in range(0, 4)]
x0.extend([0 for i in range(0, 6*2)])
res = minimize(func, x0, constraints=cons)
print(res)
