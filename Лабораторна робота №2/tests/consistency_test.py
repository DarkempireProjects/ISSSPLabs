import pandas as pd

from lab2 import IntervalMatrixPairwiseComparisons


def test_consistency():
    D_1 = pd.DataFrame([
        [(1, 1), (1, 1), (0.25, 0.5), (0.167, 0.25)],
        [(1, 1), (1, 1), (0.333, 1), (0.25, 0.5)],
        [(2, 4), (1, 3), (1, 1), (0.333, 1)],
        [(4, 6), (2, 4), (1, 3), (1, 1)]
    ])
    assert IntervalMatrixPairwiseComparisons.check_consistency_for_matrix(D_1)

    D_2 = pd.DataFrame([
        [(1, 1), (4, 6), (1, 1), (2, 4)],
        [(0, 167, 0.25), (1, 1), (0.25, 0.5), (0.25, 0.5)],
        [(1, 1), (2, 4), (1, 1), (1, 1)],
        [(0.25, 0.5), (2, 4), (1, 1), (1, 1)]
    ])

    assert not IntervalMatrixPairwiseComparisons.check_consistency_for_matrix(D_2)


test_consistency()
